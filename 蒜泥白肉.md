---
title: 蒜泥白肉
categories: 厨艺笔谈
tags: 菜谱
date: 2022/04/12 19:00:00
update: 2022/04/12 19:00:00
cover: https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220412194022.JPG
---

# 蒜泥白肉

## 食材

- 二刀肉或瘦肉一块
- 大葱或小葱
- 姜一块
- 蒜
- 黄瓜



## 准备工作

### 肉

**1、焯水**

肉冷水下锅，下入拍扁后的姜块，葱，胡椒粉一勺，料酒一小瓶盖焯水。

烧开后撇撇去浮沫，煮15-20分钟（图中的肉块需要煮20分钟）

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220412192528.png" alt="CleanShot 2022-04-12 at 19.25.15@2x" width="25%" />

时间到了后关火静待其在水中自然冷却至室温（时间来不及了也可以直接捞出，相比泡水会更干）

**2、切片**

肉泡冷却至室温后切薄片，越薄越好



### 蒜泥

**1、剁蒜泥**

半碗蒜剥好后细细剁成蒜泥，最后可以用刀背剁，最好要剁至出水。

**2、调味**

蒜泥中加入盐小半勺，糖一勺，鸡精味精适量，酱油适量，红油适量，搅拌均匀



### 黄瓜

黄瓜切丝备用

## 开始制作

> 1. 在碗底垫入黄瓜丝
> 2. 将肉片与蒜泥酱拌匀，确保每片肉上都有蒜泥
> 3. 将拌好的肉片放在黄瓜丝上，即成菜



## 最终成品

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220412193957.jpeg" alt="IMG_2152" width="45%" />

<img src="https://alfred-img.oss-cn-chengdu.aliyuncs.com/img/20220412194022.JPG" alt="IMG_2158" width="45%" />